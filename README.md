# Server Status Fork

## How do I install this thing?
Simply put the files on your server and access it from your browser. There will be a simple install dialog waiting for you.
If you prefer you can install manually by filling the info in config.php.template and renaming it to config.php.

### FOR IIS:
// Rename IISWebConfig to web.config
### FOR Apache and Nginx
// Rename ApacheHtaccess to .htaccess


## Contributing
Anyone is welcome to make pull request with new features or security patches / bug fixes.

### Translations
Any help with translations is much welcome! You can join us at https://poeditor.com/join/project/37SpmJtyOm. You can even add your own language. Just let me know when you're done and I'm going to include the language in the next release.

### Does it actually run somewhere?
Yes it does! 
* https://status.rollenspiel.monster/ (RollenspielMonster)
* https://status.nikisoft.one/ (Nikisoft/GGC)
* https://status.trucksbook.eu/ (Trucksbook)
* https://status.ecidsf.com/ (ECIDSF)
* And many more!

## FAQ

### My Translations are not working. What to do?
1. Open your shell
2. Type this command 'sudo nano /etc/locale.gen'
3. Uncomment all the languages you want.
4. Save with 'Ctrl+X'
5. Run 'sudo locale-gen'
6. Restart apache 'sudo service apache2 restart'
7. Enjoy!

### Do you have a demo page?
Yes we have! Head over to https://status2.anzah.network/admin and try the admin interface for yourself.
Login details:
```
email: sysadmin@example.com
password: Ss123456
```
Please note that changes are reverted every day.

### I noticed there is a new release. How do I update?
Updating server status is fairly straightforward. Download your config.php from the server. Delete all files. Upload the new release with config.php you downloaded earlier. You need to manually run install scripts.

### Is there any way to do this automatically?
We are working on it but it is not yet included. Stay tuned!

### Can I somehow pull status info from Server status programatically?
Yes you can! We added API to pull status data.

### Feature Request
You can write an issue and I will try to take a look when I get some time *OR* you can actually make a fork as the code it GNU licensed. Pull requests are most welcome!